from __future__ import division
from numpy import *
import numpy.random
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_mldata
from scipy.stats import mode

mnist = fetch_mldata('MNIST original')
data = mnist['data']
labels = mnist['target']

idx = numpy.random.RandomState(0).choice(70000, 11000)
train = data[idx[:10000], :].astype(int)
train_labels = labels[idx[:10000]]
test = data[idx[10000:], :].astype(int)
test_labels = labels[idx[10000:]]


def k_nn(images, image_labels, decision_image, k):
    differences = (images - decision_image)
    distances = einsum('ij, ij->i', differences, differences)
    nearest = image_labels[argsort(distances)[:k]]
    return mode(nearest)[0][0]


def _calc_knn_errors(n, k):
    errors = 0
    for i, test_image in enumerate(test):
        if test_labels[i] != k_nn(train[:n], train_labels[:n], test_image, k):
            errors += 1

    return errors / len(test)


def ex_1_b():
    print "Ex-1b: accurracy on test set %s" % (1-_calc_knn_errors(n=1000, k=10))


def ex_1_c():
    best_k = 0
    best_error = 1
    errors = []
    for k in range(1, 101):
        curr_error = _calc_knn_errors(n=1000, k=k)
        print (k, curr_error)
        errors.append((k, curr_error))
        if curr_error < best_error:
            best_error = curr_error
            best_k = k

    plt.plot([x[0] for x in errors], [x[1] for x in errors], 'b')
    plt.xlabel('k')
    plt.ylabel('Error')
    plt.savefig("ex_1_c.png")
    plt.clf()
    print "ex_1_c: Best k = %d" % (best_k,)
    return best_k


def ex_1_d(k):
    best_n = 0
    best_error = 1
    errors = []
    for n in range(100, 5001, 100):
        curr_error = _calc_knn_errors(n=n, k=k)
        print (n, curr_error)
        errors.append((n, curr_error))
        if curr_error < best_error:
            best_error = curr_error
            best_n = n

    plt.plot([x[0] for x in errors], [x[1] for x in errors], 'b')
    plt.xlabel('n')
    plt.ylabel('Error')
    plt.savefig("ex_1_d.png")
    plt.clf()
    print "ex_1_d: Best n = %d" % (best_n,)


def main():
    ex_1_b()
    best_k = ex_1_c()
    ex_1_d(best_k)

if __name__ == '__main__':
    main()
