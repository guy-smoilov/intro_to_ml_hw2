from __future__ import division
from numpy import *
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing
from sklearn.svm import LinearSVC, SVC
import matplotlib.pyplot as plt

mnist = fetch_mldata('MNIST original')
data = mnist['data']
labels = mnist['target']

neg, pos = 0,8
train_idx = numpy.random.RandomState(0).permutation(where((labels[:60000] == neg) | (labels[:60000] == pos))[0])
test_idx = numpy.random.RandomState(0).permutation(where((labels[60000:] == neg) | (labels[60000:] == pos))[0])

train_data_unscaled = data[train_idx[:6000], :].astype(float)
train_labels = (labels[train_idx[:6000]] == pos)*2-1

validation_data_unscaled = data[train_idx[6000:], :].astype(float)
validation_labels = (labels[train_idx[6000:]] == pos)*2-1

test_data_unscaled = data[60000+test_idx, :].astype(float)
test_labels = (labels[60000+test_idx] == pos)*2-1

# Preprocessing
train_data = sklearn.preprocessing.scale(train_data_unscaled, axis=0, with_std=False)
validation_data = sklearn.preprocessing.scale(validation_data_unscaled, axis=0, with_std=False)
test_data = sklearn.preprocessing.scale(test_data_unscaled, axis=0, with_std=False)


def calc_error_rate(classifier, data, lables):
    prediction = classifier.predict(data)
    errors = 0
    for i, pred in enumerate(prediction):
        if pred != lables[i]:
            errors += 1

    return errors/len(data)


def run_linearsvc(c):
    classifier = LinearSVC(C=c, loss='hinge', fit_intercept=False)
    classifier = classifier.fit(train_data, train_labels)

    training_error = calc_error_rate(classifier, train_data, train_labels)
    validation_error = calc_error_rate(classifier, validation_data, validation_labels)

    return training_error, validation_error, classifier


def ex_a():
    pows = range(-100, 101)

    min_validation_error = 1
    min_validation_error_pow = 1000
    best_classifier = None
    errors = []
    for c in pows:
        curr_training_error, curr_validation_error, classifier = run_linearsvc(10**(c/10))
        if curr_validation_error < min_validation_error:
            min_validation_error = curr_validation_error
            min_validation_error_pow = c
            best_classifier = classifier

        errors.append((curr_training_error, curr_validation_error))
        print (c, errors[-1])

    print "Ex-3a: Best C is 10^%s. It has %s validation error" % (min_validation_error_pow/10, min_validation_error)
    plt.semilogx([10**(x/10) for x in pows], [x[0] for x in errors], 'b', label='Training Error')
    plt.semilogx([10**(x/10) for x in pows], [x[1] for x in errors], 'r', label='Validation Error')
    plt.xlabel('C')
    plt.ylabel('Errors')
    plt.legend(loc='best', shadow=True, fontsize='x-large')
    plt.savefig("ex3_a.png")
    plt.clf()
    return best_classifier


def ex_c(classifier):
    plt.imshow(reshape(classifier.coef_[0], (28, 28)), interpolation="nearest")
    plt.savefig("ex3_c.png")
    plt.clf()


def ex_d(classifier):
    test_error = calc_error_rate(classifier, test_data, test_labels)
    print "Ex-3d: Best C test error is %s" % (test_error,)


def ex_e():
    classifier = SVC(kernel='rbf', C=10, gamma=5*(10**-7))
    classifier = classifier.fit(train_data, train_labels)
    training_error = calc_error_rate(classifier, train_data, train_labels)
    test_error = calc_error_rate(classifier, test_data, test_labels)
    print "Ex-3e: Training-set error: %s, Test-set error: %s" % (training_error, test_error)


def main():
    best_classifier = ex_a()
    ex_c(best_classifier)
    ex_d(best_classifier)
    ex_e()

if __name__ == '__main__':
    main()
