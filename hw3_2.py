from __future__ import division
from numpy import *
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing
import matplotlib.pyplot as plt
import pandas

mnist = fetch_mldata('MNIST original')
data = mnist['data']
labels = mnist['target']

neg, pos = 0,8
train_idx = numpy.random.RandomState(0).permutation(where((labels[:60000] == neg) | (labels[:60000] == pos))[0])
test_idx = numpy.random.RandomState(0).permutation(where((labels[60000:] == neg) | (labels[60000:] == pos))[0])

train_data_unscaled = data[train_idx[:6000], :].astype(float)
train_labels = (labels[train_idx[:6000]] == pos)*2-1

validation_data_unscaled = data[train_idx[6000:], :].astype(float)
validation_labels = (labels[train_idx[6000:]] == pos)*2-1

test_data_unscaled = data[60000+test_idx, :].astype(float)
test_labels = (labels[60000+test_idx] == pos)*2-1

# Preprocessing
train_data = sklearn.preprocessing.scale(train_data_unscaled, axis=0, with_std=False)
validation_data = sklearn.preprocessing.scale(validation_data_unscaled, axis=0, with_std=False)
test_data = sklearn.preprocessing.scale(test_data_unscaled, axis=0, with_std=False)


def calc_accuracy(w, X, Y):
    failures_indexes = []
    errors = 0
    for i, x in enumerate(X):
        if (dot(X[i], w) * Y[i]) <= 0:
            errors += 1
            failures_indexes.append(i)

    return errors/len(X), failures_indexes


def shuffle(X, Y):
    c = numpy.c_[X.reshape(len(X), -1), Y.reshape(len(Y), -1)]
    X2 = c[:, :X.size // len(X)].reshape(X.shape)
    Y2 = c[:, X.size // len(X):].reshape(Y.shape)
    numpy.random.shuffle(c)
    return X2, Y2


def normalize(v):
    norm = linalg.norm(v)
    if norm == 0:
        return v
    return v / norm


def perceptron(X, Y):
    w = zeros(len(X[0]))
    for i, x in enumerate(X):
        if (dot(X[i], w)*Y[i]) <= 0:
            w = w + X[i]*Y[i]
    return w


def ex_a(normalized_train_data, normalized_test_data):
    n_list = [5, 10, 50, 100, 500, 1000, 5000]
    accuracies = {}
    for n in n_list:
        accuracies[n] = []
        X = normalized_train_data[:n]
        Y = train_labels[:n]
        for i in range(100):
            X2, Y2 = shuffle(X, Y)
            w = perceptron(X2, Y2)
            error_rate, _ = calc_accuracy(w, normalized_test_data, test_labels)
            accuracies[n].append(1-error_rate)
    results = {'mean': [], "5% percentile": [], "95% percentile": []}
    for n in accuracies:
        results['mean'].append(mean(accuracies[n]))
        results['5% percentile'].append(percentile(accuracies[n], 5))
        results['95% percentile'].append(percentile(accuracies[n], 95))

    print "Ex-2a:"
    df = pandas.DataFrame(results, n_list)
    print df


def ex_b(normalized_train_data):
    w = perceptron(normalized_train_data, train_labels)
    plt.imshow(reshape(w, (28, 28)), interpolation="nearest")
    plt.savefig("ex2_b.png")
    plt.clf()
    return w


def ex_c(w, normalized_test_data):
    error_rate, failures_indexes = calc_accuracy(w, normalized_test_data, test_labels)
    print "Ex-2c: Accuracy over test set: %s" % (1-error_rate)
    return test_data_unscaled[failures_indexes[0]], test_data_unscaled[failures_indexes[1]]


def ex_d(failed_sample1, failed_sample2):
    plt.imshow(reshape(failed_sample1, (28, 28)), interpolation="nearest")
    plt.savefig("ex2_d_1.png")
    plt.clf()
    plt.imshow(reshape(failed_sample2, (28, 28)), interpolation="nearest")
    plt.savefig("ex2_d_2.png")
    plt.clf()


def main():
    normalized_train_data = array([normalize(x) for x in train_data])
    normalized_test_data = array([normalize(x) for x in test_data])

    ex_a(normalized_train_data, normalized_test_data)
    w = ex_b(normalized_train_data)
    smpl1, smpl2 = ex_c(w, normalized_test_data)
    ex_d(smpl1, smpl2)


if __name__ == '__main__':
    main()
