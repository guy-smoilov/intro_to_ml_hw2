from numpy import *
from numpy.random import choice
from random import uniform
import matplotlib.pyplot as plt
import sys

def find_best_interval(xs, ys, k):
    assert all(array(xs) == array(sorted(xs))), "xs must be sorted!"

    xs = array(xs)
    ys = array(ys)
    m = len(xs)
    P = [[None for j in range(k+1)] for i in range(m+1)]
    E = zeros((m+1, k+1), dtype=int)

    # Calculate the cumulative sum of ys, to be used later
    cy = concatenate([[0], cumsum(ys)])

    # Initialize boundaries:
    # The error of no intervals, for the first i points
    E[:m+1,0] = cy

    # The minimal error of j intervals on 0 points - always 0. No update needed.

    # Fill middle
    for i in range(1, m+1):
        for j in range(1, k+1):
            # The minimal error of j intervals on the first i points:

            # Exhaust all the options for the last interval. Each interval boundary is marked as either
            # 0 (Before first point), 1 (after first point, before second), ..., m (after last point)
            options = []
            for l in range(0,i+1):
                next_errors = E[l,j-1] + (cy[i]-cy[l]) + concatenate([[0], cumsum((-1)**(ys[arange(l, i)] == 1))])
                min_error = argmin(next_errors)
                options.append((next_errors[min_error], (l, arange(l,i+1)[min_error])))

            E[i,j], P[i][j] = min(options)

    # Extract best interval set and its error count
    best = []
    cur = P[m][k]
    for i in range(k,0,-1):
        best.append(cur)
        cur = P[cur[0]][i-1]
        if cur == None:
            break
    best = sorted(best)
    besterror = E[m,k]

    # Convert interval boundaries to numbers in [0,1]
    exs = concatenate([[0], xs, [1]])
    representatives = (exs[1:]+exs[:-1]) / 2.0
    intervals = [(representatives[l], representatives[u]) for l,u in best]

    return intervals, besterror


def generate_pair():
    x = round(uniform(0, 1), 5)
    if 0 <= x <= 0.25 or 0.5 <= x <= 0.75:
        y = choice(2, 1, p=[0.2, 0.8])[0]
    else:
        y = choice(2, 1, p=[0.9, 0.1])[0]
    return x, y


def generate_m_pairs(m):
    pairs = []
    for i in xrange(m):
        pairs.append(generate_pair())
    return sorted(pairs)

def find_best_intervals_for_pairs(pairs, k):
    return find_best_interval([x[0] for x in pairs], [x[1] for x in pairs], k)

def intersection_size(interval1, interval2):
    return max(0, min(interval1[1], interval2[1]) - max(interval1[0], interval2[0]))

def intervals_size(intervals):
    return sum([i[1] - i[0] for i in intervals])

label_one_intervals = [(0,0.25),(0.5,0.75)]
label_one_intervals_size = intervals_size(label_one_intervals)
label_zero_intervals = [(0.25,0.5),(0.75,1)]
label_one_prob = 0.8
label_zero_prob = 0.9

def calc_true_error(intervals):
    total_size = intervals_size(intervals)
    
    true_label_one = sum([intersection_size(i1, i2) for i1 in intervals for i2 in label_one_intervals])
    false_label_one = total_size - true_label_one
    false_label_zero = label_one_intervals_size - true_label_one
    true_label_zero = 1 - (true_label_one + false_label_one + false_label_zero)
    
    true_error = \
        (1-label_one_prob) * true_label_one + \
        label_zero_prob * false_label_one + \
        (1-label_zero_prob) * true_label_zero + \
        label_one_prob * false_label_zero
    
    return true_error

def calc_errors(xs, ys, intervals):
    assert all(array(xs) == array(sorted(xs))), "xs must be sorted!"
    intervals = sorted(intervals)

    errors = 0
    current_interval_index = 0
    for i, x in enumerate(xs):
        # find the current most relevant interval
        while x > intervals[current_interval_index][1] and current_interval_index < len(intervals) - 1:
            current_interval_index += 1

        if (x < intervals[current_interval_index][0] or x > intervals[current_interval_index][1]) and ys[i] == 1:
            errors += 1

        elif intervals[current_interval_index][0] <= x <= intervals[current_interval_index][1] and ys[i] == 0:
            errors += 1

    return errors


def plot_with_intervals(xs, ys, intervals):
    plt.plot(xs, ys, '.', color='gray')
    plt.plot([0.25, 0.25], [-0.5, 1.5], 'g--', linewidth=0.5)
    plt.plot([0.5, 0.5], [-0.5, 1.5], 'g--', linewidth=0.5)
    plt.plot([0.75, 0.75], [-0.5, 1.5], 'g--', linewidth=0.5)
    for interval in intervals:
        plt.plot([interval[0], interval[1]], [0.5, 0.5], 'r-', linewidth=10)
    plt.axis([0, 1, -0.5, 1.5])
    plt.savefig("ex1_a.png")
    plt.clf()


def ex1_a():
    pairs = generate_m_pairs(100)
    intervals,_ = find_best_interval([x[0] for x in pairs], [x[1] for x in pairs], 2)
    plot_with_intervals([x[0] for x in pairs], [x[1] for x in pairs], intervals)


def ex1_c():
    emp_results = []
    test_results = []
    for m in range(10, 101, 5):
        m_emp_error_sum = 0
        m_true_error_sum = 0
        for i in range(100):
            training_pairs = generate_m_pairs(m)
            intervals, emp_error = find_best_interval([x[0] for x in training_pairs], [x[1] for x in training_pairs], 2)
            m_emp_error_sum += emp_error/float(m)
            true_error = calc_true_error(intervals)
            m_true_error_sum += true_error
        emp_results.append((m, m_emp_error_sum/100.0))
        test_results.append((m, m_true_error_sum/100.0))

    plt.plot([x[0] for x in emp_results], [x[1] for x in emp_results], 'b', label='Empirical Error')
    plt.plot([x[0] for x in test_results], [x[1] for x in test_results], 'r', label='True Error')
    plt.xlabel('m')
    plt.ylabel('Errors')
    plt.legend(loc='best', shadow=True, fontsize='x-large')
    plt.savefig("ex1_c.png")
    plt.clf()


def ex1_d():
    m = 50
    max_k = 20
    emp_results = []
    test_results = []
    k_to_intervals = {}
    best_k = -1
    best_emp_error = m
    training_pairs = generate_m_pairs(m)
    for k in range(1, max_k+1):
        intervals, emp_error = find_best_interval([x[0] for x in training_pairs], [x[1] for x in training_pairs], k)
        if emp_error < best_emp_error:
            best_emp_error = emp_error
            best_k = k
        k_to_intervals[k] = intervals
        emp_results.append((k, emp_error/float(m)))
        true_error = calc_true_error(intervals)
        test_results.append((k, true_error))
    plt.plot([x[0] for x in emp_results], [x[1] for x in emp_results], 'b', label='Empirical Error')
    plt.plot([x[0] for x in test_results], [x[1] for x in test_results], 'r', label='True Error')
    plt.xlabel('k')
    plt.ylabel('Errors')
    plt.legend(loc='best', shadow=True, fontsize='x-large')

    plt.savefig("ex1_d.png")
    plt.clf()
    print "ex1_d: best k - %d" % (best_k,)
    return k_to_intervals


def ex1_e(k_to_intervals):
    m = 50
    max_k = 20
    validation_results = []
    validation_pairs = generate_m_pairs(m)
    best_k = -1
    best_error = m

    for k in range(1, max_k+1):
        intervals = k_to_intervals[k]
        validation_error = calc_errors([x[0] for x in validation_pairs], [x[1] for x in validation_pairs], intervals)
        if validation_error < best_error:
            best_error = validation_error
            best_k = k

        validation_results.append((k, validation_error/float(m)))
    plt.plot([x[0] for x in validation_results], [x[1] for x in validation_results], 'b', label='Validation Error')
    plt.xlabel('k')
    plt.ylabel('Errors')
    plt.legend(loc='best', shadow=True, fontsize='x-large')

    plt.savefig("ex1_e.png")
    plt.clf()
    print "ex1_e: best k - %d" % (best_k,)


def main():
    if sys.argv[1] == 'a':
        ex1_a()
    elif sys.argv[1] == 'c':
        ex1_c()
    elif sys.argv[1] == 'de':
        k_to_intervals = ex1_d()
        ex1_e(k_to_intervals)
    else:
        print "bad commandline"

if __name__ == '__main__':
    main()
